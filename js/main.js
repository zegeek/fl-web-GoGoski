//menu
function onClickResp() {
    var x = document.getElementById("mytopnav");
    var y = document.getElementById("dark-bck");
    if (x.className === "topnav") {
        x.className += " responsive-topnav";
        y.className += " dark-bck-displayer";
    } else {
        x.className = "topnav";
        y.className = "dark-background-menu";
    }
}
$(document).ready(function() {
    $('.generic-button').click(function() {
        var buttonID = $(this).attr('id');
        var currentClasses = $(this).attr('class');
        console.log(currentClasses);
        if(currentClasses == "generic-button disabled-button")
        {
            if(buttonID == "disp-current")
            {
                $(this).attr('class','generic-button actif-button');
                $('#disp-done').attr('class','generic-button disabled-button');
                $('#complete-activities').hide('fast');
                $('#current-activities').show('fast');
            }
            else if(buttonID == "disp-done")
            {
                $(this).attr('class','generic-button actif-button');
                $('#disp-current').attr('class','generic-button disabled-button');
                $('#current-activities').hide('fast');
                $('#complete-activities').show('fast');
            }
        }
    });
    //modal handling
    $('#modal-loader-1').load("../mod/edit_profile_mod.html");
    $('#modal-loader-2').load("../mod/add_sport_mod.html");
    $('#modal-loader-3').load("../mod/edit_about_mod.html");
    $('#modal-loader-4').load("../mod/edit_passwrd_mod.html");
    $('#modal-loader-connect').load("../mod/connect_mod.html");
    $('#modal-loader-inscription').load("../mod/inscription_mod.html");
    $('#modal-loader-notif').load("../mod/notifications_mod.html");
    $('#open-connect-modal').click(function() {
        $('#connect-modal').css('display','block');
    });
    $('#open-inscription-modal').click(function() {
        $('#inscription-modal').css('display','block');
    });
    $('#open-edit-modal').click(function() {
        $('#edit-profile-modal').css('display','block');
    });
    $('#open-add-sport-modal').click(function() {
        $('#add-sport-modal').css('display','block');
    });
    $('#open-edit-about-modal').click(function() {
        $('#edit-about-modal').css('display','block');
    });
    $('#open-edit-passwrd-modal').click(function() {
        $('#edit-passwrd-modal').css('display','block');
    });
    $('#open-notifications-modal').click(function(e) {
        e.preventDefault();
        $('#notifications-modal').css('display','block');
    });
    $('.message-box').click(function() {
        var box_state = $(this).attr('clicked');
        if(box_state == "not")
        {
            $('.message-box').not($(this)).attr('clicked','not').css('background-color','#ffffff');
            $('.message-box').not($(this)).children('.friend-box-image').css('border','none');
            $('.message-box').not($(this)).children('.friend-box-element').children(".friend-box-info-line").css('color','#9a9a9a');
            $('.message-box').not($(this)).children('.friend-box-element').children(".friend-box-info-line").children('.name-emphasise').css("color","black");
            $('.message-box').not($(this)).children('.friend-box-element').children(".message-box-time").css("color","black");
            $('.message-box').not($(this)).children('.friend-box-element').children(".friend-box-action-element-cont").children(".delete-box").css("background-color","#7e7e7e");
            $(this).attr('clicked','yes').css('background-color','#63a4fe');
            $(this).children('.friend-box-image').css('border','2px solid white');
            $(this).children('.friend-box-element').children(".friend-box-info-line").css("color","white");
            $(this).children('.friend-box-element').children(".friend-box-info-line").children('.name-emphasise').css("color","white");
            $(this).children('.friend-box-element').children(".message-box-time").css("color","white");
            $(this).children('.friend-box-element').children(".friend-box-action-element-cont").children(".delete-box").css("background-color","white");
        }
    });
    $('.message-box .friend-box-element .friend-box-action-element-cont .delete-box').click(function() {
        $(this).parent().parent().parent('.message-box').hide('fast').remove();
    });
    $('.sport-display .sport-default-check .delete-box').click(function() {
        $(this).parent().parent('.sport-display').hide('fast').remove();
    });
    $('.map-tag:not(.map-tag-prev)').click(function() {
        $('.map-tag').children('.map-pin').hide();
        $(this).children('.map-pin').toggle();//css("display","block");
    });
});